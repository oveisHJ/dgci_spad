# -*- coding: utf-8 -*-
"""
Created on Thu Jan 31 16:42:42 2019

@author: Oveis
"""

# Importing libraries
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import RandomizedSearchCV, KFold, cross_val_score, train_test_split
from sklearn.preprocessing import StandardScaler


#importing the dataset
df01=pd.read_csv('All_Indices_0701.csv')
df01=df01[df01['Plot Number']!=103]


#extracting X and y from dataset(July 1st)
first=0
last=3
X01=df01.iloc[:,first:last]
#X01['var']=df01['Variety']
y01=df01.loc[:,'SPAD']
x_DGCI01=df01.iloc[:,8:9]
df15=pd.read_csv('All_Indices_0715.csv')

#for July 15 file several columns need to be dropped
df15=df15.drop(df15.iloc[:,14:],axis=1)

#extracting X and y from dataset(July 15th)
X15=df15.iloc[:,first:last]
#X15['var']=df15['Variety']
y15=df15.loc[:,'SPAD']
x_DGCI15=df15.iloc[:,8:9]

#Creating final X and y
y=pd.concat([y01,y15])
y=np.array(y)
X=pd.concat([X01,X15])
x_DGCI=pd.concat([x_DGCI01,x_DGCI15]).values.reshape(-1,1)

#Standarize the dataset
scaler=StandardScaler()
X=scaler.fit_transform(X)
y=np.array(y).reshape(-1,1)
y=scaler.fit_transform(y)
y=y.ravel()
#add interaction terms
#interactions=PolynomialFeatures(interaction_only=True)
#X=interactions.fit_transform(X)

X_train, X_test, y_train, y_test=train_test_split(X, y, test_size=0.25, random_state=13)
from sklearn.svm import SVR
def reg_svm(C=1,kernel='poly',coef0=1, degree=3):
    reg=SVR(C=C, kernel=kernel,coef0=coef0,degree=degree)
    #reg.fit(X_train, y_train)
    #y_pred=reg.predict(X_test)
    #print("R Square for SVR is {}".format(reg.score(X_test,y_test)))
    #print("mean square error for SVR is {}".format(mean_squared_error(y_test,y_pred)))
    return reg
reg_svm(C=0.01, coef0=1, degree=3)
from sklearn.svm import LinearSVR
reg_svr=LinearSVR(C=1, epsilon=0.3, loss='squared_epsilon_insensitive', random_state=2)
cv=cross_val_score(reg_svm(),X,y,cv=10)
print(cv.mean())
