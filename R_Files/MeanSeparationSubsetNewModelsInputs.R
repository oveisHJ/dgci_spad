library(agricolae)

# New models viz: SLR_RGB*, SLR_DGCI*, PR_RGB*, PR_DGCI* and 
# New groups RGB*, DGCI*, RGB, DGCI, and Both were considered


#### R^2

rm(list=ls())
setwd(".")

data.in <- read.csv('External_cv_all4.csv',header=TRUE)
summary(data.in)
data.in<-subset(data.in, (Method=='Test'))
summary(data.in)


model<-aov(Rsqr~models, data=data.in)
duncan.test(model, "models",console=TRUE)
#HSD.test(model, "models",console=TRUE) #Tukey’s HSD (Honestly Significant Differences) Test  


model<-aov(Rsqr~Inputs, data=data.in)
duncan.test(model, "Inputs",console=TRUE)
#HSD.test(model, "Inputs",console=TRUE) #Tukey’s HSD (Honestly Significant Differences) Test  


model<-aov(Rsqr~Inputs*models, data=data.in)
duncan.test(model, c("Inputs","models"),console=TRUE)


#### RMSE

data.RMSE <- read.csv('External_cv_RMSE4.csv',header=TRUE)
data.RMSE$RMSE<- sqrt(data.RMSE$RMSE)
summary(data.RMSE)

model<-aov(RMSE~models, data=data.RMSE)
duncan.test(model, "models",console=TRUE)
#HSD.test(model, "models",console=TRUE) #Tukey’s HSD (Honestly Significant Differences) Test  

model<-aov(RMSE~Inputs, data=data.RMSE)
duncan.test(model, "Inputs",console=TRUE)
#HSD.test(model, "Inputs",console=TRUE) #Tukey’s HSD (Honestly Significant Differences) Test  

model<-aov(RMSE~Inputs*models, data=data.RMSE)
duncan.test(model, c("Inputs","models"),console=TRUE)


