# -*- coding: utf-8 -*-
"""
Created on Thu Sep 13 18:03:34 2018

@author: Oveis
"""

import numpy as np
from sklearn import model_selection
from sklearn.ensemble import RandomForestRegressor
import pandas as pd
from sklearn.metrics import mean_squared_error
from sklearn.linear_model import LinearRegression
from sklearn.utils import shuffle
import statsmodels.formula.api as sm
def nested_cross_validation(X, y, reg_rf,reg_svm,x_DGCI, x_DGCI_poly, parameters_rf,parameters_svm, inner_split, outter_split, n_iter=10,scoring='neg_mean_squared_error'):
    
    X, x_DGCI, x_DGCI_poly, y = shuffle(X, x_DGCI, x_DGCI_poly, y, random_state=13)
    outter=model_selection.KFold(n_splits=outter_split, shuffle=False, random_state=53)
    
   

    df_list_rf=[]
    df_list_svm=[]
    dgci_coef=[]
    scores=[]
    #X_List=[]
    external_cv=[]
    for train_index_outer, test_index_outer in outter.split(X):
        scores=[]
        X_train_outer, X_test_outer = X[train_index_outer], X[test_index_outer]
        #X_List.append(X_test_outer) # to see if the loop produce the same results everytime. Just for test
        y_train_outer, y_test_outer = y[train_index_outer], y[test_index_outer]
        x_DGCI_train, x_DGCI_test = x_DGCI[train_index_outer], x_DGCI[test_index_outer]
        x_DGCI_poly_train, x_DGCI_poly_test = x_DGCI_poly[train_index_outer], x_DGCI_poly[test_index_outer]
        
        
        randomSearch=model_selection.RandomizedSearchCV(reg_rf, param_distributions=parameters_rf, scoring=scoring, cv=inner_split, n_iter=n_iter, random_state=42, return_train_score=True)
        randomSearch.fit(X_train_outer, y_train_outer)
        print(randomSearch.best_params_)
        df_list_rf.append(pd.DataFrame(randomSearch.cv_results_))
        
        rf=randomSearch.best_estimator_
        rf.fit(X_train_outer, y_train_outer)
        y_pred=rf.predict(X_test_outer)
        print("R Square for training is {}".format(rf.score(X_train_outer,y_train_outer)))
        scores.append(rf.score(X_train_outer,y_train_outer))
        print("R Square for test is     {}".format(rf.score(X_test_outer,y_test_outer)))
        scores.append(rf.score(X_test_outer,y_test_outer))
        print("mean square error is {}".format(mean_squared_error(y_test_outer,y_pred)))
        scores.append(mean_squared_error(y_test_outer,y_pred))
        print('*************************')
        
        randomSearch=model_selection.RandomizedSearchCV(reg_svm, param_distributions=parameters_svm, scoring=scoring, cv=inner_split, n_iter=n_iter, random_state=42, return_train_score=True)
        randomSearch.fit(X_train_outer, y_train_outer)
        print(randomSearch.best_params_)
        df_list_svm.append(pd.DataFrame(randomSearch.cv_results_))
        
        reg_svm=randomSearch.best_estimator_
        reg_svm.fit(X_train_outer, y_train_outer)
        y_pred=reg_svm.predict(X_test_outer)
        print("R Square for training SVM is {}".format(reg_svm.score(X_train_outer,y_train_outer)))
        print("R Square for test is     {}".format(reg_svm.score(X_test_outer,y_test_outer)))
        print("mean square error is {}".format(mean_squared_error(y_test_outer,y_pred)))
        
        
        scores.append(reg_svm.score(X_train_outer,y_train_outer))
        scores.append(reg_svm.score(X_test_outer,y_test_outer))
        scores.append(mean_squared_error(y_test_outer,y_pred))
        print('*************************')
        
        
        
        lin_reg=LinearRegression()
        poly_reg=LinearRegression()
        
        lin_reg.fit(x_DGCI_train, y_train_outer)
        lin_coef=list(lin_reg.coef_)
        lin_intercept=lin_reg.intercept_
        lin_coef.append(lin_intercept)
        dgci_coef.append(lin_coef)
        y_DGCI_pred=lin_reg.predict(x_DGCI_test)
        print("R Square for training Linear Regression is {}".format(lin_reg.score(x_DGCI_train,y_train_outer)))
        print("R Square for test is     {}".format(lin_reg.score(x_DGCI_test,y_test_outer)))
        print("mean square error is {}".format(mean_squared_error(y_test_outer,y_DGCI_pred)))
        
        scores.append(lin_reg.score(x_DGCI_train,y_train_outer))
        scores.append(lin_reg.score(x_DGCI_test,y_test_outer))
        scores.append(mean_squared_error(y_test_outer,y_DGCI_pred))
        print('*************************')
        
        poly_reg.fit(x_DGCI_poly_train, y_train_outer)
        y_DGCI_poly_pred=poly_reg.predict(x_DGCI_poly_test)
        print("R Square for training Polynomial Regression is {}".format(poly_reg.score(x_DGCI_poly_train,y_train_outer)))
        print("R Square for test is     {}".format(poly_reg.score(x_DGCI_poly_test,y_test_outer)))
        print("mean square error is {}".format(mean_squared_error(y_test_outer,y_DGCI_poly_pred)))
        
        scores.append(poly_reg.score(x_DGCI_poly_train,y_train_outer))
        scores.append(poly_reg.score(x_DGCI_poly_test,y_test_outer))
        scores.append(mean_squared_error(y_test_outer,y_DGCI_poly_pred))
        print('*************************')
        
        M_lin_reg=LinearRegression()
        M_lin_reg.fit(X_train_outer, y_train_outer)
        y_pred=M_lin_reg.predict(X_test_outer)
        print("R Square for training Multiple Linear Regression is {}".format(M_lin_reg.score(X_train_outer,y_train_outer)))
        print("R Square for test is     {}".format(M_lin_reg.score(X_test_outer,y_test_outer)))
        print("mean square error is {}".format(mean_squared_error(y_test_outer,y_pred)))
        
        X_opt=np.append(arr=np.ones((len(X_train_outer),1)).astype(int), values=X_train_outer, axis=1)
        X_sel=X_opt[:,[0,1,2,3]]
        reg_ols=sm.OLS(endog=y_train_outer, exog=X_sel).fit()
        reg_ols.pvalues
        
        scores.append(M_lin_reg.score(X_train_outer,y_train_outer))
        scores.append(M_lin_reg.score(X_test_outer,y_test_outer))
        scores.append(mean_squared_error(y_test_outer,y_pred))
        scores.append(list(reg_ols.pvalues))
        print('*******************************************************&&&&&&&&&&&&&&&&&&&&&&&&&&&&*********************')
        
        external_cv.append(scores)
    return df_list_rf, dgci_coef, df_list_svm,external_cv


if __name__=='__main__':
                
    
    parameters= {'n_estimators':list(range(25,51)),
                 'max_depth':list(range(4,11)),
                 'min_samples_leaf':list(range(2,11))
                }
    
    #nested=nested_cross_validation(X, y, reg, parameters,inner_split=4, outter_split=6,n_iter=5, scoring='r2')
        
    import pandas as pd
    import numpy as np
    import matplotlib.pyplot as plt
    from sklearn.metrics import mean_squared_error
    from sklearn.model_selection import RandomizedSearchCV, KFold, cross_val_score, train_test_split
    from sklearn.preprocessing import StandardScaler
    
    
    
    #importing the dataset
    df01=pd.read_csv('All_Indices_0701.csv')
    df01=df01[df01['Plot Number']!=103]
    
    
    #extracting X and y from dataset(July 1st)
    first=0
    last=3
    X01=df01.iloc[:,first:last]
    X01=df01[['R1','R2','R3','R4']]
    #X01['var']=df01['Variety']
    y01=df01.loc[:,'SPAD']
    x_DGCI01=df01.iloc[:,8:9]
    df15=pd.read_csv('All_Indices_0715.csv')
    
    #for July 15 file several columns need to be dropped
    #df15=df15.drop(df15.iloc[:,14:],axis=1)
    
    #extracting X and y from dataset(July 15th)
    X15=df15.iloc[:,first:last]
    #X15['var']=df15['Variety']
    X15=df15[['R1','R2','R3','R4']]
    y15=df15.loc[:,'SPAD']
    x_DGCI15=df15.iloc[:,8:9]
    # =============================================================================
    #to normalize the pixel counts
    def normalize(x):
        sum_of_row=sum(x)
        for i in range(len(x)):
            x[i]=x[i]*100/sum_of_row
    
        return x
    X01=X01.apply(normalize,axis=1)
    X15=X15.apply(normalize,axis=1)
    # =============================================================================
    #Creating final X and y
    y=pd.concat([y01,y15])
    y=np.array(y)
    X=pd.concat([X01,X15])
    x_DGCI=pd.concat([x_DGCI01,x_DGCI15]).values.reshape(-1,1)
    
    #Standarize the dataset
    scaler=StandardScaler()
    X=scaler.fit_transform(X)
    
    #add interaction terms
    #interactions=PolynomialFeatures(interaction_only=True)
    #X=interactions.fit_transform(X)

    rf=RandomForestRegressor(random_state=13)
    from sklearn.linear_model import LinearRegression
    singleLinearD=LinearRegression()
    #y=np.array(y)
    #nested=nested_cross_validation(X, y, rf, parameters, inner_split=10, outter_split=10, n_iter=20, scoring='r2')
    #outter=model_selection.KFold(n_splits=10, shuffle=True, random_state=13)
    #for train_index_outer, test_index_outer in outter.split(X):
        #X_train_outer, X_test_outer = X[train_index_outer], X[test_index_outer]
        #y_train_outer, y_test_outer = y[train_index_outer], y[test_index_outer]
    
    nested, X_list=nested_cross_validation(X, y, rf,x_DGCI, singleLinearD, parameters, inner_split=10, outter_split=10, n_iter=7, scoring='r2')