# -*- coding: utf-8 -*-
"""
Created on Thu Sep 13 18:03:34 2018

@author: Oveis
"""

import numpy as np
from sklearn import model_selection
import pandas as pd
from sklearn.metrics import mean_squared_error
from sklearn.utils import shuffle



def randomized_search(X_train, X_test, y_train, y_test, reg, parameters, scoring, inner_split=10, n_iter=2):
    
    scores=[]
    regName=str(type(reg)).split('.')[-1][:-2]
    
    randomSearch=model_selection.RandomizedSearchCV(reg, param_distributions=parameters, scoring=scoring, cv=inner_split, n_iter=n_iter, random_state=42, return_train_score=True)
    randomSearch.fit(X_train, y_train)
    print(randomSearch.best_params_)
    cv_results = pd.DataFrame(randomSearch.cv_results_)
    
    best_reg=randomSearch.best_estimator_
    best_reg.fit(X_train, y_train)
    y_pred=best_reg.predict(X_test)
    
    print('*'*20,regName,'*'*20)
    
    print("R Square for training is {}".format(best_reg.score(X_train,y_train)))
    scores.append(best_reg.score(X_train,y_train))
    print("R Square for test is     {}".format(best_reg.score(X_test,y_test)))
    scores.append(best_reg.score(X_test,y_test))
    print("mean square error is {}".format(np.sqrt(mean_squared_error(y_test,y_pred))))
    scores.append(np.sqrt(mean_squared_error(y_test,y_pred)))
    print('*************************')
    
    return (cv_results, scores)


def nested_cross_validation(X, y, reg, inner_split, outter_split, n_iter=10, parameters=None ,scoring='neg_mean_squared_error'):
    
    X, y = shuffle(X,y, random_state=13)
    
    outter=model_selection.KFold(n_splits=outter_split, shuffle=False, random_state=53)
    
    dgci_coef=[]
    external_cv=[]
    #X_List2=[]
    cv_result_list=[]
    scores_list=[]
    
    for train_index_outer, test_index_outer in outter.split(X):

        
        
        X_train, X_test = X[train_index_outer], X[test_index_outer]
        y_train, y_test = y[train_index_outer], y[test_index_outer]
        #X_List2.append(X_test) # to see if the loop produce the same results everytime. Just for test
        
        if str(type(reg)).split('.')[-1][:-2] in ['RandomForestRegressor','SVR']:
            
            cv_results, scores = randomized_search(X_train, X_test, y_train, y_test, reg, parameters, scoring, inner_split=inner_split, n_iter=n_iter)
            cv_result_list.append(cv_results)
            scores_list.append(scores)
            
        else:
            
            scores=[]
            
            reg.fit(X_train, y_train)
            lin_coef=list(reg.coef_)
            lin_intercept=reg.intercept_
            lin_coef.append(lin_intercept)
            train_score=reg.score(X_train,y_train)
            lin_coef.append(train_score)
            dgci_coef.append(lin_coef)
            
            y_pred=reg.predict(X_test)
            
            print("R Square for training is {}".format(reg.score(X_train,y_train)))
            print("R Square for test is     {}".format(reg.score(X_test,y_test)))
            print("mean square error is {}".format(np.sqrt(mean_squared_error(y_test,y_pred))))
            
            scores.append(reg.score(X_train,y_train))
            scores.append(reg.score(X_test,y_test))
            scores.append(np.sqrt(mean_squared_error(y_test,y_pred)))
            
            print('*************************')
            
            
            external_cv.append(scores)
            
    return cv_result_list, dgci_coef, external_cv,scores_list