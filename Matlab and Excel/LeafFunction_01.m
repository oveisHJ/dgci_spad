close all
clear all
clc
prompt = {'Enter the number of first photo you want to analyze','Enter the number of last photo you want to analyze','Enter photos overall name'};
dlg_title = 'Input';
num_lines = 1;
def = {'801','830','32'};
photo_no = inputdlg(prompt,dlg_title,num_lines,def);
mmm=str2double(photo_no{1});
nnn=str2double(photo_no{2});
range=mmm;
for k=mmm:nnn
    
    jpgFilename= strcat('C:\Image Processing\2015 IDC\Leonard 15th July\328\',photo_no{3}, num2str(k), '.jpg');
    im = imread(jpgFilename);
    im=imrotate(im,180);    % For July 15th. Since green and yellow disks are at right of the pictures
r=im(:,:,1);g=im(:,:,2);b=im(:,:,3);
%%
%Detecting PinkBoard
bwPink=zeros(size(g));
bwPink(g<100 & r>100)=1;
bwPink=bwareaopen(bwPink,500000);
bwPink=imfill(bwPink,'holes');
bwPink=imopen(bwPink,strel('square',20));
bwPink=bwareaopen(bwPink,500000);
Prop=regionprops(bwPink,'BoundingBox');
r(bwPink==0)=0;g(bwPink==0)=0;b(bwPink==0)=0;
im=cat(3,r,g,b);
im=imcrop(im,Prop.BoundingBox);
%%
%Here im is a cropped image of the original one.
r=im(:,:,1);g=im(:,:,2);b=im(:,:,3);
hsv=rgb2hsv(im);
h=hsv(:,:,1); s=hsv(:,:,2); v=hsv(:,:,3);
DGCI=(((360*h-60)/60)+(1-s)+(1-v))/3;
DGCIg=DGCI;
DGCIg(DGCIg>0.9)=0;

%%
%Detecting Green Disk
bwGreen=im2bw(DGCIg,0.4);
bwGreen=bwareaopen(bwGreen,50000);
imshow(bwGreen), title 'Green'
figure
Gstat=bwconncomp(bwGreen);
DGCIg(bwGreen==0)=0;
DGCIg=mean(nonzeros(DGCIg));
gMaxPixel=max(Gstat.PixelIdxList{1});
%%
%Detecting Yellow Disk
bwYellow=zeros(size(s));
bwYellow(s>0.9)=1;
bwYellow=imopen(bwYellow,strel('disk',25));
bwYellow=bwareaopen(bwYellow,5000);
imshow(bwYellow), title 'Yellow'
figure
DGCIy=DGCI;
DGCIy(bwYellow==0)=0;
DGCIy=mean(nonzeros(DGCIy));
Ystat=bwconncomp(bwYellow);
yMaxPixel=max(Ystat.PixelIdxList{1});
%%
%Detecting Leaf_ 2nd Method
maximum=max(gMaxPixel,yMaxPixel);
rLeaf=r;
rLeaf(1:maximum*1.2)=0;
rLeaf(0.95*end:end)=0;
rLeaf(s<0.2 | h>0.4)=0;
bwLeaf=im2bw(rLeaf,graythresh(rLeaf));
bwLeaf=imfill(bwLeaf,'holes');
bwLeaf=imerode(bwLeaf,strel('disk',5));
bwLeaf=bwareaopen(bwLeaf,4000);
StatLeaf=regionprops(bwLeaf,'BoundingBox');
r(bwLeaf==0 | h>0.6)=0;g(bwLeaf==0 | h>0.6)=0;b(bwLeaf==0 | h>0.6)=0;
pic=cat(3,r,g,b);
pic=imcrop(pic,StatLeaf.BoundingBox);
imshow(pic)

end