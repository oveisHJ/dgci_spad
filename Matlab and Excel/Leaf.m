function [pic,DGCIg,DGCIy]=Leaf(im)
% im=imread('C:\Image Processing\2015 IDC\Leonard 1st July\102\10201.jpg');

r=im(:,:,1);g=im(:,:,2);b=im(:,:,3);
%%
%Detecting PinkBoard
bwPink=zeros(size(g));
bwPink(g<100 & r>100)=1;
bwPink=bwareaopen(bwPink,500000);
bwPink=imfill(bwPink,'holes');
bwPink=imopen(bwPink,strel('square',20));
bwPink=bwareaopen(bwPink,500000);
Prop=regionprops(bwPink,'BoundingBox');
r(bwPink==0)=0;g(bwPink==0)=0;b(bwPink==0)=0;
im=cat(3,r,g,b);
im=imcrop(im,Prop.BoundingBox);
%%
%Here im is a cropped image of the original one.
r=im(:,:,1);g=im(:,:,2);b=im(:,:,3);
hsv=rgb2hsv(im);
h=hsv(:,:,1); s=hsv(:,:,2); v=hsv(:,:,3);
DGCI=(((360*h-60)/60)+(1-s)+(1-v))/3;
DGCIg=DGCI;
DGCIg(DGCIg>0.9)=0;

%%
%Detecting Green Disk
bwGreen=im2bw(DGCIg,0.5);           %0.5 for 222 15th    0.44 otherwise
bwGreen=bwareaopen(bwGreen,50000);
% imtool(bwGreen)
Gstat=bwconncomp(bwGreen);
DGCIg(bwGreen==0)=0;
DGCIg=mean(nonzeros(DGCIg));
gMaxPixel=max(Gstat.PixelIdxList{1});
%%
%Detecting Yellow Disk
bwYellow=zeros(size(s));
bwYellow(s>0.9)=1;
bwYellow=imopen(bwYellow,strel('disk',25));
bwYellow=bwareaopen(bwYellow,5000);
DGCIy=DGCI;
DGCIy(bwYellow==0)=0;
DGCIy=mean(nonzeros(DGCIy));
Ystat=bwconncomp(bwYellow);
yMaxPixel=max(Ystat.PixelIdxList{1});
%%
%Detecting Leaf_ 2nd Method
maximum=max(gMaxPixel,yMaxPixel);
rLeaf=r;
rLeaf(1:maximum*1.2)=0;
rLeaf(0.95*end:end)=0;
rLeaf(s<0.2 | h>0.4)=0;
bwLeaf=im2bw(rLeaf,graythresh(rLeaf));
bwLeaf=imfill(bwLeaf,'holes');
bwLeaf=imerode(bwLeaf,strel('disk',3));
bwLeaf=bwareaopen(bwLeaf,4000);
StatLeaf=regionprops(bwLeaf,'BoundingBox');
r(bwLeaf==0 | h>0.6)=0;g(bwLeaf==0 | h>0.6)=0;b(bwLeaf==0 | h>0.6)=0;
pic=cat(3,r,g,b);
pic=imcrop(pic,StatLeaf.BoundingBox);
end

