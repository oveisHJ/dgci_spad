% Yellow:HSV: 50 87 91  DGCI:  0.0178        RGB: 231 199 31 #e7c71f
% Yellow:HSV: 66 88 100 DGCI:  0.0733       RGB: 231 255 32  
% Green: HSV: 91 38 42  DGCI:  0.5722       RGB: 87 108 67  #576c43
  
%DGCI=(((50-60)/60)+(1-0.87)+(1-0.91))/3

clear all
close all
clc
prompt = {'Enter the number of first photo you want to analyze','Enter the number of last photo you want to analyze','Enter photos overall name'};
dlg_title = 'Input';
num_lines = 1;
def = {'401','430','10'};
photo_no = inputdlg(prompt,dlg_title,num_lines,def);
mmm=str2double(photo_no{1});
nnn=str2double(photo_no{2});
range=mmm;
for k=mmm:nnn
    jpgFilename= strcat('C:\Image Processing\2015 IDC\Leonard 15th July\104\',photo_no{3}, num2str(k), '.jpg');
    im = imread(jpgFilename);
    im=imrotate(im,180);    % For July 15th. Since green and yellow disks are at right of the pictures
% im=imread('C:\Image Processing\2015 IDC\Leonard 1st July\101\10101.jpg');
[pic,DGCIg,DGCIy]=Leaf(im);

imshow(pic), title (num2str(k));
%Here hsv is the values for cropped image of leaves
hsv=rgb2hsv(pic);
h=hsv(:,:,1);s=hsv(:,:,2);v=hsv(:,:,3);
%%
[DGCI,DGCIcorrected,DGCIcorrected_2_avg,DGCIcorrected_2]=DarkGreenColorIndex(h,s,v,DGCIg,DGCIy);
%%
%min DGCI -> 0.15 , max DGCI -> 0.48

r=pic(:,:,1);g=pic(:,:,2);b=pic(:,:,3);
Red=mean(nonzeros(r));Green=mean(nonzeros(g));Blue=mean(nonzeros(b));
Hue=mean(nonzeros(h))*360;Saturation=mean(nonzeros(s));Value=mean(nonzeros(v));

GB=g-b;
RG=r-g;
GR=g-r;                                   %No good results
GBRG=GB./RG;
ExG=2*g-r-b;
ExR=1.3*r-g;                                %ExR=1.4*r-g
CIVE=0.441*r-0.811*g+0.385*b+18.78745;
ExGR=ExG-ExR;
NGRDI=(r-g)./(g+r);                         %the original is (g-r)./(g+r);      No result
COM1=ExG+CIVE+ExGR;
MExG=1.262*g-0.884*r-0.311*b;
GRAY=0.2898 * r + 0.5870*g + 0.1140*b;
RBI=(r-b)./(r+b);
ERI=(r-g).*(r-b);
EGI=GR.*GB;
EBI=(b-g).*(b-r);
% EVI=(2.5*(g-r))./(g+6*r-7.5*b+1);
% VEG=g./((r.^0.667).*(b.^0.333));          %I could not make it work
NDI=(((g-r)./(g+r))+1)*128;               %No good result
% NDI2=(((g-r)./(g+r))+1)*128;              %No good result
%%
rStar=im2double(r);gStar=im2double(g);bStar=im2double(b);
% r=r/255;g=g/255;b=b/255;          %imdouble in the line above will divide the matrices by 255
X=(rStar+gStar+bStar);
rn=rStar./X;gn=gStar./X;bn=bStar./X;

GB_=gn-bn;
RG_=rn-gn;
GR_=gn-rn;                              %No good results
GBRG_=GB_./RG_;
NDI_=(((gn-rn)./(gn+rn))+1)*128;
NDI2_=(((gn-bn)./(gn+bn))+1)*128;
ExG_=2*gn-rn-bn;
ExR_=1.3*rn-gn;                                 %ExR=1.4*r-g;     
CIVE_=0.441*rn-0.811*gn+0.385*bn+18.78745;
ExGR_=ExG_-ExR_;
NGRDI_=(rn-gn)./(gn+rn);                         %the original is (g-r)./(g+r);
VEG_=gn./((rn.^0.667).*(bn.^0.333));             %I could not make it to work  
COM1_=ExG_+CIVE_+ExGR_;
MExG_=1.262*gn-0.884*rn-0.311*bn;
GRAY_=0.2898 * rn + 0.5870*gn + 0.1140*bn;
RBI_=(rn-bn)./(rn+bn);
ERI_=(rn-gn).*(rn-bn);
EGI_=GR_.*GB_;
EBI_=(bn-gn).*(bn-rn);
% EVI_=(2.5*(gn-rn))./(gn+6*rn-7.5*bn+1);


% subplot(1,2,1), imshow (EVI)
% subplot(1,2,2), imshow (EVI_), title 'Double'
%%
%Average Values
% Calculate average for Rn, Gn, Bn
GB_(isnan(GB_))=0;
RG_(isnan(RG_))=0;
GR_(isnan(GR_))=0;
GBRG_(isnan(GBRG_))=0;
NDI2_(isnan(NDI2_))=0;
ExG_(isnan(ExG_))=0; ExR_(isnan(ExR_))=0; CIVE_(isnan(CIVE_))=0;
ExGR_(isnan(ExGR_))=0; NGRDI_(isnan(NGRDI_))=0; VEG_(isnan(VEG_))=0;
COM1_(isnan(COM1_))=0; MExG_(isnan(MExG_))=0; GRAY_(isnan(GRAY_))=0; RBI_(isnan(RBI_))=0; ERI_(isnan(ERI_))=0; EGI_(isnan(EGI_))=0; EBI_(isnan(EBI_))=0; 
NDI_(isnan(NDI_))=0;
%%
GB=mean(nonzeros(GB));GB_=mean(nonzeros(GB_));
RG=mean(nonzeros(RG));RG_=mean(nonzeros(RG_));
GR=mean(nonzeros(GR));GR_=mean(nonzeros(GR_));
GBRG=mean(nonzeros(GBRG));GBRG_=mean(nonzeros(GBRG_));
NDI=mean(nonzeros(NDI));NDI_=mean(nonzeros(NDI_));
% NDI2_=mean(nonzeros(NDI2_));
ExG=mean(nonzeros(ExG));ExG_=mean(nonzeros(ExG_));
ExR=mean(nonzeros(ExR));ExR_=mean(nonzeros(ExR_));
CIVE=mean(nonzeros(CIVE));CIVE_=mean(nonzeros(CIVE_));
ExGR=mean(nonzeros(ExGR));ExGR_=mean(nonzeros(ExGR_));
NGRDI=mean(nonzeros(NGRDI));NGRDI_=mean(nonzeros(NGRDI_));
VEG_=mean(nonzeros(VEG_));
COM1=mean(nonzeros(COM1));COM1_=mean(nonzeros(COM1_));
MExG=mean(nonzeros(MExG));MExG_=mean(nonzeros(MExG_));
GRAY=mean(nonzeros(GRAY));GRAY_=mean(nonzeros(GRAY_));
RBI=mean(nonzeros(RBI));RBI_=mean(nonzeros(RBI_));
ERI=mean(nonzeros(ERI));ERI_=mean(nonzeros(ERI_));
EGI=mean(nonzeros(EGI));EGI_=mean(nonzeros(EGI_));
EBI=mean(nonzeros(EBI));EBI_=mean(nonzeros(EBI_));

%%
rn(isnan(rn))=0;
gn(isnan(gn))=0;
bn(isnan(bn))=0;
RedDouble=mean(nonzeros(rn));
GreenDouble=mean(nonzeros(gn));
BlueDouble=mean(nonzeros(bn));
%%


Range=strcat(num2str(k-range+2));
input=[DGCIg,DGCI,DGCIcorrected,DGCIcorrected_2,Red,RedDouble,Blue,BlueDouble,Green,GreenDouble,GB,GB_, RG, RG_,GR,GR_,GBRG,GBRG_,ExG,ExG_,ExR,ExR_,CIVE,CIVE_,ExGR,ExGR_,NGRDI,NGRDI_,VEG_,COM1,COM1_,MExG,MExG_,GRAY,GRAY_,RBI,RBI_,ERI,ERI_,EGI,EGI_,EBI,EBI_];

xlswrite('publish_ranges.xlsx',input,'Sheet1',Range);
end