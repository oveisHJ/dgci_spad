function [DGCI, DGCIcorrected,DGCIcorrected_2_avg,DGCIcorrected_2]=DarkGreenColorIndex(h,s,v,DGCIg,DGCIy)
    DGCI=(((h*360-60)/60)+(1-s)+(1-v))/3;
    % Yellow:HSV: 66 88 100 DGCI:  0.0733       RGB: 231 255 32  
    Slope=0.4989/(DGCIg-DGCIy);
    Yintercept=0.5722-(Slope*DGCIg);
    DGCIcorrected=Slope*DGCI+Yintercept;
    %%    
    % Yellow:HSV: 50 87 91  DGCI:  0.0178        RGB: 231 199 31 #e7c71f
    Slope=0.5544/(DGCIg-DGCIy);
    Yintercept=0.5722-(Slope*DGCIg);
    DGCIcorrected_2=Slope*DGCI+Yintercept;
    %%
    %Pink board reflection was visible through some leaves. Therefore this
    %section turn them into zero.
        DGCI(DGCI<0)=0;
    DGCI=mean(nonzeros(DGCI));
        DGCIcorrected(DGCIcorrected<0)=0;
    DGCIcorrected=mean(nonzeros(DGCIcorrected));
        DGCIcorrected_2(h<=0 | DGCIcorrected_2<=0)=0;
    DGCIcorrected_2_avg=mean(nonzeros(DGCIcorrected_2));
end