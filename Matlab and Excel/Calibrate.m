function [pic]= Calibrate(im,bwGreen,bwYellow)

r=im(:,:,1);g=im(:,:,2);b=im(:,:,3);
%%
%Extracting RGB of Yellow disk to compare with known values.
rYellow=r;gYellow=g;bYellow=b;
rYellow(bwYellow==0)=0;gYellow(bwYellow==0)=0;bYellow(bwYellow==0)=0;
rYellow=(mean2(nonzeros(rYellow)));
gYellow=(mean2(nonzeros(gYellow)));
bYellow=(mean2(nonzeros(bYellow)));

%%
%Extracting RGB of Green disk to compare with known values
rGreen=r;gGreen=g;bGreen=b;
rGreen(bwGreen==0)=0;gGreen(bwGreen==0)=0;bGreen(bwGreen==0)=0;
rGreen=(mean2(nonzeros(rGreen)));
gGreen=(mean2(nonzeros(gGreen)));
bGreen=(mean2(nonzeros(bGreen)));

%%
coefficients = polyfit([rGreen,rYellow], [87, 231], 1);
mRed = coefficients (1); cRed = coefficients (2);

coefficients = polyfit([gGreen,gYellow], [108, 199], 1);
mGreen = coefficients (1); cGreen = coefficients (2);


coefficients = polyfit([bGreen,bYellow], [67, 31], 1);
mBlue = coefficients (1); cBlue = coefficients (2);

r=mRed*r+cRed;
g=mGreen*g+cGreen;
b=mBlue*b+cBlue;
pic=cat(3,r,g,b);

end
