# Importing libraries
import pandas as pd
import numpy as np
from sklearn.preprocessing import StandardScaler, PolynomialFeatures
from sklearn.svm import SVR
from sklearn.ensemble import RandomForestRegressor
import CrossValidation_2 as CrossValidation
from sklearn.linear_model import LinearRegression

print('************** Change indices for x_DGCI01 and x_DGCI15 ****************')
#importing the dataset
df01=pd.read_csv('All_Indices_0701.csv')
df01=df01[df01['Plot Number']!=103]


#extracting X and y from dataset(July 1st)
first=0
last=3
X01=df01.iloc[:,first:last]
X01=df01[['R1','R2','R3','R4']]
y01=df01.loc[:,'SPAD']
x_DGCI01=df01.iloc[:,9:10]
df15=pd.read_csv('All_Indices_0715.csv')

#for July 15 file several columns need to be dropped
#df15=df15.drop(df15.iloc[:,14:],axis=1)

#extracting X and y from dataset(July 15th)
X15=df15.iloc[:,first:last]
X15=df15[['R1','R2','R3','R4']]
y15=df15.loc[:,'SPAD']
x_DGCI15=df15.iloc[:,9:10]
# =============================================================================
#to normalize the pixel counts
def normalize(x):
    sum_of_row=sum(x)
    for i in range(len(x)):
        x[i]=x[i]*100/sum_of_row

    return x


X01=X01.apply(normalize,axis=1)
X15=X15.apply(normalize,axis=1)
#adding rgb to the input variables
X01=pd.concat([df01.iloc[:,first:last],X01],axis=1)
X15=pd.concat([df15.iloc[:,first:last],X15],axis=1)
# =============================================================================
#Creating final X and y
y=pd.concat([y01,y15])
y=np.array(y)
X=pd.concat([X01,X15])
x_DGCI=pd.concat([x_DGCI01,x_DGCI15]).values.reshape(-1,1)
poly_reg=PolynomialFeatures(degree=2)
x_DGCI_poly=poly_reg.fit_transform(x_DGCI)
#Standarize the dataset
scaler=StandardScaler()
X=scaler.fit_transform(X)



if __name__=='__main__':
    

    
    randomForest=RandomForestRegressor(random_state=13)

    parameters_rf= {'n_estimators':list(range(25,51)),
              'max_depth':list(range(4,11)),
              'min_samples_leaf':list(range(2,11))
                }
    
    
    reg_svm=SVR()
    
    parameters_svm= {'kernel':['poly','rbf'],
      'C':list(np.linspace(0.1,2.1,11)),
      'coef0':list(np.linspace(0,2,11)),
      'degree':[2,3,4,5,6],
      'gamma':[0.01,0.1]
      }
        



    nested_rf=CrossValidation.nested_cross_validation(X, y, randomForest,  inner_split=10, outter_split=10, n_iter=50,parameters=parameters_rf, scoring='r2')
    rf_df=pd.DataFrame(nested_rf[3])
    rf_cv=[]
    for cv in nested_rf[0]:
    
        rf_cv.append(cv[cv.rank_test_score==1][['param_max_depth', 'param_min_samples_leaf', 'param_n_estimators','mean_train_score','mean_test_score']].values[0])
    
    rf_cv=pd.DataFrame(rf_cv)
    rf_cv.columns=['max_depth','min_sample_leaf','n_estimator','mean_train_score','mean_test_score']
    rf_cv.to_csv('rf_inner_cv2.csv')
    
    
    nested_svm=CrossValidation.nested_cross_validation(X, y, reg_svm,      inner_split=10, outter_split=10, n_iter=50, parameters=parameters_svm, scoring='r2')
    svm_df=pd.DataFrame(nested_svm[3])
    svm_cv=[]
    for cv in nested_svm[0]:
    
        svm_cv.append(cv[cv.rank_test_score==1][['param_C', 'param_coef0', 'param_degree', 'param_kernel','param_gamma','mean_train_score','mean_test_score']].values[0])

    svm_cv=pd.DataFrame(svm_cv)
    svm_cv.columns=['C', 'coef0', 'degree', 'kernel','gamma','mean_train_score','mean_test_score']
    svm_cv.to_csv('svm_inner_cv2.csv')

    
    reg_lin=LinearRegression()
    linear_scores=CrossValidation.nested_cross_validation(x_DGCI, y, reg_lin, inner_split=10, outter_split=10, n_iter=50, scoring='r2')
    linear_df=pd.DataFrame(linear_scores[2])
    poly_scores=CrossValidation.nested_cross_validation(x_DGCI_poly, y, reg_lin, inner_split=10, outter_split=10, n_iter=50, scoring='r2')
    poly_df=pd.DataFrame(poly_scores[2])
    mlr_scores=CrossValidation.nested_cross_validation(X, y, reg_lin, inner_split=10, outter_split=10, n_iter=50, scoring='r2')
    mlr_df=pd.DataFrame(mlr_scores[2])
    
    external_cv=pd.concat([rf_df,svm_df,linear_df,poly_df, mlr_df],axis=1)
    external_cv.columns=['rf_train','rf_test','rf_rmse','svm_train','svm_test','svm_rmse','linear_train','linear_test','linear_rmse','polynomial_train','polynomial_test','polynomial_rmse','mulitple_linear_train','mulitple_linear_test','mulitple_linear_rmse']    
    external_cv.to_csv('External_cv2.csv')
    
#======================================================================================
#======================================================================================

import statsmodels.formula.api as sm
X_opt=np.append(arr=np.ones((689,1)).astype(int), values=X, axis=1)
X_sel=X_opt[:,[0,1,3,4,5,6]]
reg_ols=sm.OLS(endog=y, exog=X_sel).fit()
reg_ols.pvalues
reg_ols.summary()
