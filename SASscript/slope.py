import pandas as pd
import numpy as np
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
df01=pd.read_csv('07012015_Leonard_All_Indicies.csv')
df01=df01[df01.Plot!=103]
df15=pd.read_csv('07152015_Leonard_All_Indicies.csv')
df15=df15[df15.Plot!=103]

reg01=LinearRegression()
reg15=LinearRegression()
reg01.fit(df01.DGCI_C2.values.reshape(-1,1), df01.SPAD)
reg15.fit(df15.DGCI_C2.values.reshape(-1,1), df15.SPAD)
print('slope july 1 is',reg01.coef_)
print('slope july 15 is',reg15.coef_)

data=pd.concat([df01,df15])
data.reset_index(inplace=True,drop=True)
reg=LinearRegression()
reg.fit(data.DGCI_C2.values.reshape(-1,1), data.SPAD)
print(reg.coef_,'\n****************')
print(reg.intercept_,'\n****************')
reg.score(data.DGCI_C2.values.reshape(-1,1), data.SPAD)
y_pred=reg.predict(data.DGCI_C2.values.reshape(-1,1))
rmse=np.sqrt(mean_squared_error(data.SPAD, y_pred))
for i in sorted(df01.Plot.unique()):
    df=df15[df15.Plot==i]
    X = df.DGCI_C2.values.reshape(-1,1)
    y = df.SPAD
    reg=LinearRegression()
    reg.fit(X, y)
    print(i,reg.coef_,round(reg.intercept_,2), round(reg.score(X, y),3))
    
