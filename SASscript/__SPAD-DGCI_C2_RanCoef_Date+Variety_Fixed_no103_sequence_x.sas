**************************************************************;
/*   HLM_SPADvDGCI2_m1.sas 

Author: Curt Doetkott
Directory: C:\Statistics\userproj\Jalilian, Oveis\
Purpose: assess relationships among measured variables

Input Data Files --------------------------------------------
 07012015_Leonard_All_Indicies.txt - from spreadsheet
 07152015_Leonard_All_Indicies.txt - from spreadsheet
 
Input Variables ---------------------------------------------
 Red   Green   Hue  Value  
 DGCI = Corrected Dark Green Color Index
 DGCI_C
 DGCI_C2
 GB   RG   RG_   GR_   GBRG   NDI_   ExR   ExR_
 NGRDI_   GRAY   ERI   ERI_   EGI_  
 Sample
 SPAD = SPAD meter reading (Chlorophyll surrogate)
 Plot
 Rep
 Variety = Soybean variety 
 
Created Variables -------------------------------------------
 ds = data set (1=July 1 or 2=July 15)
*/
**************************************************************;

options ls=220 ps=70 formchar = "|----|+|---+=|-/\<>*";

title1 'SPAD vs DGCI_2 -- Oveis Jalilian';

data july_1 (drop=jnk:);
  infile '07012015_Leonard_All_Indicies.csv' missover dlm=',' dsd firstobs=2;
  DS=1;
  input  DGCI_C2  Sample  SPAD  Plot  Rep  Variety;
    if plot='103' then SPAD=.;  *** Reset SPAD values that are incorrect to missing. ***;
  ;;;;

data july_15 (drop=jnk:);
  infile '07152015_Leonard_All_Indicies.csv' missover dlm=',' dsd firstobs=2;
  DS=2;
  input  DGCI_C2  Sample  SPAD  Plot  Rep  Variety;
	if plot='103' then SPAD=.;  *** Reset SPAD values that are incorrect to missing. ***;
  ;;;;

data all;
  set july_1 july_15;
   if plot='103' then delete;
  run;

ods html path="C:\Users\Oveis\Desktop\dgci_spad\SASscript\Oveis"
         gpath="C:\Users\Oveis\Desktop\dgci_spad\SASscript\Oveis";
ods listing gpath="C:\Users\Oveis\Desktop\dgci_spad\SASscript\Oveis";

ods graphics on;
proc sgplot;
  by DS;
  scatter y=SPAD x=DGCI_C2 / group=Variety FilledOutlinedMarkers markerattrs=(symbol=circlefilled);
  title1 ' ';
  run;

proc sgplot;
  by DS;
  scatter y=SPAD x=DGCI_C2 / group=Plot FilledOutlinedMarkers markerattrs=(symbol=circlefilled);
  reg y=SPAD x=DGCI_C2 / group=Plot;
  title1 ' ';
  Label DS='Date'
        DGCI_C2='Corrected DGCI';
  run;

ods pdf file="SPAD vs Corrected DGCI_C2_Sequence_No103.pdf";  
proc mixed data=all covtest cl scoring=8;
  class DS;
  model SPAD = / solution ddfm=satterthwaite;
  random int / subject=DS type=UN;
  title2 'HLM Model 1 -- Estimate ICC for Date';
  run;

proc mixed data=all covtest cl scoring=8;
  class Variety;
  model SPAD = / solution ddfm=satterthwaite;
  random int / subject=Variety type=UN;
  title2 'HLM Model 1 -- Estimate ICC for Variety';
  run;

proc mixed data=all covtest cl scoring=8;
  class Plot;
  model SPAD = / solution ddfm=satterthwaite;
  random int / subject=Plot type=UN;
  title2 'HLM Model 1 -- Estimate ICC for Plot';
  run;

proc mixed data=all covtest cl scoring=8
     plots(only)=(STUDENTPANEL BoxPlot(observed fixed subject));
  class DS Plot;
  model SPAD = / solution ddfm=satterthwaite;
  random int / subject=DS type=VC;
  random int / subject=Plot(DS) type=VC;
  title2 'HLM Model 1 -- Estimate ICCs for Date, Plot(Date) Simultaneously';
  run;

proc mixed data=all covtest cl scoring=8
     plots(only)=(STUDENTPANEL BoxPlot(observed fixed subject));
  class DS Plot;
  model SPAD = DGCI_C2 / solution ddfm=satterthwaite;
  random int DGCI_C2 / subject=DS type=VC;
  random int DGCI_C2 / subject=Plot(DS) type=VC;
  title2 'HLM Model 2 -- Estimates Including Random Ints + Slopes';
  run;

proc mixed data=all covtest cl scoring=8
     plots(only)=(STUDENTPANEL BoxPlot(observed fixed subject));
  class DS Plot Variety;
  model SPAD = DGCI_C2 Variety/ solution ddfm=satterthwaite;
  random int DGCI_C2 / subject=DS type=VC;
  random int DGCI_C2 / subject=Plot(DS) type=VC;
  title2 'HLM Model 2 -- Estimates Including Random Ints + Slopes, Variety Fixed';
  run;
/* *** maybe needs HPMIXED ??? ***;
proc mixed data=all covtest cl scoring=8
     plots(only)=(STUDENTPANEL BoxPlot(observed fixed subject));
  class DS Plot Variety;
  model SPAD = DGCI_C2 Variety DGCI_C2*Variety/ solution ddfm=satterthwaite;
  random int DGCI_C2 / subject=DS type=VC solution;
  random int DGCI_C2 / subject=Plot(DS) type=VC solution;
  title2 'HLM Model 2 -- Estimates Including Random Ints + Slopes, Variety+Var*DGCI_2 Fixed';
  run;
*/
proc mixed data=all covtest cl scoring=8
     plots(only)=(STUDENTPANEL BoxPlot(observed fixed subject));
  class DS Plot Variety;
  model SPAD = DGCI_C2 Variety DS / solution ddfm=kr;
  random int DGCI_C2 / subject=Plot(DS) type=UN solution;
  title2 'HLM Final Model - Dates, Varieties Fixed, Plot(Date) Random Ints+Slopes';
  run; 

ods pdf close;
