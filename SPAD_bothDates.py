# Importing libraries
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import RandomizedSearchCV, KFold, cross_val_score, train_test_split
from sklearn.preprocessing import StandardScaler, PolynomialFeatures
from sklearn.svm import SVR

print('************** Change indices for x_DGCI01 and x_DGCI12 ****************')
#importing the dataset
df01=pd.read_csv('All_Indices_0701.csv')
df01=df01[df01['Plot Number']!=103]


#extracting X and y from dataset(July 1st)
first=0
last=3
X01=df01.iloc[:,first:last]
X01=df01[['R1','R2','R3','R4']]
#X01['var']=df01['Variety']
y01=df01.loc[:,'SPAD']
x_DGCI01=df01.iloc[:,3:4]
df15=pd.read_csv('All_Indices_0715.csv')

#for July 15 file several columns need to be dropped
#df15=df15.drop(df15.iloc[:,14:],axis=1)

#extracting X and y from dataset(July 15th)
X15=df15.iloc[:,first:last]
#X15['var']=df15['Variety']
X15=df15[['R1','R2','R3','R4']]
y15=df15.loc[:,'SPAD']
x_DGCI15=df15.iloc[:,3:4]
# =============================================================================
#to normalize the pixel counts
def normalize(x):
    sum_of_row=sum(x)
    for i in range(len(x)):
        x[i]=x[i]*100/sum_of_row

    return x
X01=X01.apply(normalize,axis=1)
X15=X15.apply(normalize,axis=1)
#adding rgb to the input variables
X01=pd.concat([df01.iloc[:,first:last],X01],axis=1)
X15=pd.concat([df15.iloc[:,first:last],X15],axis=1)
# =============================================================================
#Creating final X and y
y=pd.concat([y01,y15])
y=np.array(y)
X=pd.concat([X01,X15])
x_DGCI=pd.concat([x_DGCI01,x_DGCI15]).values.reshape(-1,1)
poly_reg=PolynomialFeatures(degree=2)
x_DGCI_poly=poly_reg.fit_transform(x_DGCI)
#Standarize the dataset
scaler=StandardScaler()
X=scaler.fit_transform(X)
#y=scaler.fit_transform(np.array(y).reshape(-1,1)).ravel()
#add interaction terms
#interactions=PolynomialFeatures(interaction_only=True)
#X=interactions.fit_transform(X)


def assessment(reg, X, y, title, scoring='neg_mean_squared_error'):
    
    X_train, X_test, y_train, y_test=train_test_split(X, y, test_size=0.2, random_state=13)    
    reg.fit(X_train, y_train)    
    y_pred=reg.predict(X_test)
    print("R Square for {} is {}".format(title, reg.score(X_test,y_test)))
    print("mean square error for {} is {}".format(title, mean_squared_error(y_test,y_pred)))
    cv=cross_val_score(reg, X_train, y_train, cv=10, scoring=scoring)
    if scoring!='r2':
        cv=-cv
    cv=np.sqrt(cv)
    print(cv)
    print("average of cross_validation is {}".format(cv.mean()))
    print("Standard deviation of cross_validation is {}".format(cv.std()))
    
def inner(reg, X, y, title,  parameters,n_iter=10,scoring='neg_mean_squared_error'):
    
    inner_cv = KFold(n_splits=5, shuffle=True, random_state=13)        
    randomSearch=RandomizedSearchCV(reg,param_distributions=parameters,scoring=scoring, cv=inner_cv, n_jobs=-1, n_iter=n_iter, random_state=13, return_train_score=False)
    randomSearch.fit(X, y)
    print('ineer done')
    return randomSearch

def outter(reg, X, y, title, scoring='neg_mean_squared_error'):
    
    outter_cv = KFold(n_splits=5, shuffle=True, random_state=13)
    nested_score = cross_val_score(reg, X, y, cv=outter_cv, scoring=scoring)
    print('outter done')
    return nested_score


if __name__=='__main__':
    

    from sklearn.ensemble import RandomForestRegressor
    randomForest=RandomForestRegressor(random_state=13)

    #assessment(randomForest, X, y, "Random Forest",'r2')
    #assessment(singleLinearD, x_DGCI, y,"Linear Regression",'r2')    

    parameters= {'n_estimators':list(range(25,51)),
              'max_depth':list(range(4,11)),
              'min_samples_leaf':list(range(2,11))
                }
    
    reg_svm=SVR()
    parameters_svm= {'kernel':['poly','rbf'],
      'C':list(np.linspace(0.1,2.1,11)),
      'coef0':list(np.linspace(0,2,11)),
      'degree':[2,3,4,5,6],
      'gamma':[0.01,0.1]
      }
        
    reg_svm=SVR()
'''
    rand=inner(randomForest, X, y, 'Random Forest', parameters, 4,'r2')
    df=pd.DataFrame(rand.cv_results_)
        
    nested_orig=outter(rand, X, y, 'Random Forest', 'r2')
    
'''
import CrossValidation

nested=CrossValidation.nested_cross_validation(X, y, randomForest, reg_svm, x_DGCI,x_DGCI_poly, parameters,parameters_svm, inner_split=10, outter_split=10, n_iter=2, scoring='r2')
rf_cv=[]
for cv in nested[0]:
    
    rf_cv.append(cv[cv.rank_test_score==1][['param_max_depth', 'param_min_samples_leaf', 'param_n_estimators','mean_train_score','mean_test_score']].values[0])
rf_cv=pd.DataFrame(rf_cv)
rf_cv.columns=['max_depth','min_sample_leaf','n_estimator','mean_train_score','mean_test_score']
rf_cv.to_csv('rf_inner_cv.csv')

svm_cv=[]
for cv in nested[2]:
    
    svm_cv.append(cv[cv.rank_test_score==1][['param_C', 'param_coef0', 'param_degree', 'param_kernel','param_gamma','mean_train_score','mean_test_score']].values[0])
svm_cv=pd.DataFrame(svm_cv)
svm_cv.columns=['C', 'coef0', 'degree', 'kernel','gamma','mean_train_score','mean_test_score']
svm_cv.to_csv('svm_inner_cv.csv')

external_cv=pd.DataFrame(nested[3])
external_cv.columns=['rf_train','rf_test','rf_rmse','svm_train','svm_test','svm_rmse','linear_train','linear_test','linear_rmse','polynomial_train','polynomial_test','polynomial_rmse','mulitple_linear_train','mulitple_linear_test','mulitple_linear_rmse','MLR_pvalues']    
external_cv.to_csv('External_cv.csv')
# =============================================================================
# randomForest=RandomForestRegressor(random_state=13,n_estimators=5,max_depth=9, min_samples_leaf=5) 
# parameters= [{'n_estimators':[20,40,50,100,200,300,400],
#              'max_depth':[2,3,4,5,6,7,8,12],
#              'min_samples_leaf':[2,4,6,8,10]}
#             ]
# gridSearch=GridSearchCV(randomForest,param_grid=parameters,scoring='r2', cv=10)
# gridSearch.fit(X,y)
# print(gridSearch.score)
# print(gridSearch.best_estimator_)
# print(gridSearch.best_params_)
# =============================================================================
#======================================================================================
#======================================================================================
#Multiple linear regression assumption check
#1 outcome should have a linear relationship with all predictors
#y_ass=singleLinear.predict(X)
#plt.scatter(X.iloc[:,0],y_ass,c='r')
#plt.scatter(X.iloc[:,1],y_ass,c='g')
#plt.scatter(X.iloc[:,2],y_ass,c='b')
#plt.show()
#2 residuals should be normally distributed
#y_residual=y_ass-y
#plt.hist(y_residual)
#plt.show()
"""
plt.scatter(y,singleLinear.predict(X.iloc[:,:last]),c='r')
plt.scatter(y,randomForest.predict(X.iloc[:,:last]),c='b')

plt.scatter(y,singleLinearD.predict(x_DGCI),c='g')

plt.legend()
plt.show()
"""
'''
regTitle='AdaBoost'
from sklearn.ensemble import AdaBoostRegressor
reg=AdaBoostRegressor(DecisionTreeRegressor(random_state=45,max_depth=4),n_estimators=100,learning_rate=0.1,loss='square', random_state=45)
reg.fit(X_train, y_train)
y_predada=reg.predict(X_test)
print("R Square for {} is {}".format(regTitle, reg.score(X_test,y_test)))
print("mean square error for {} is {}".format(regTitle, mean_squared_error(y_test,y_predada)))
'''
#======================================================================================
#======================================================================================
#======================================================================================

import statsmodels.formula.api as sm
X_opt=np.append(arr=np.ones((689,1)).astype(int), values=X, axis=1)
X_sel=X_opt[:,[0,4,5,6]]
reg_ols=sm.OLS(endog=y, exog=X_sel).fit()
reg_ols.pvalues
reg_ols.summary()
'''
from sklearn.linear_model import LinearRegression
reg=LinearRegression()
reg.fit(x_DGCI,y)
h=list(reg.coef_)
h.append(reg.intercept_)
'''