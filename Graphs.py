# -*- coding: utf-8 -*-
"""
Created on Fri Feb  8 00:18:44 2019

@author: Oveis
"""
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from sklearn.linear_model import LinearRegression
reg=LinearRegression()
#df=pd.read_csv(r'C:\Users\Oveis\Desktop\dgci_spad\model_scores\AllModels\External_cv_all.csv',index_col=['models','input','set'])
df=pd.read_csv(r'./model_scores/AllModels/External_cv_all_rgb.csv')
df=pd.read_csv(r'./model_scores/AllModels/External_cv_RMSE_rgb.csv')
df.RMSE=np.sqrt(df.RMSE)
df['ordered_models']=pd.Categorical(df['model'],
  categories=['Linear Regression','Linear Regression_2','Polynomial', 'Polynomial_2','Multiple Linear Regression','SVM','Random Forest'],
  ordered=True)
df['ordered_input']=pd.Categorical(df['input'],
  categories=['DGCI classes and RGB','DGCI classes','RGB','Average RGB'],
  ordered=True)
df.sort_values(['ordered_input','ordered_models'],inplace=True)

df.loc[df.model=='Multiple Linear Regression','model']='MLR'
df.loc[df.model=='Linear Regression','model']='SLR'
df.loc[df.model=='Random Forest','model']='RF'

df_nested=df.drop(df[df.model=='SLR'].index)
df_nested.drop(df_nested[df_nested.model=='Polynomial'].index,inplace=True)
df.drop(df[df.model=='MLR'].index,inplace=True)
df.drop(df[df.model=='RF'].index,inplace=True)
df.drop(df[df.model=='SVM'].index,inplace=True)

df.sort_index(inplace=True)

df01=pd.read_csv('All_Indices_0701.csv')
df15=pd.read_csv('All_Indices_0715.csv')
plots=list(df01['Plot Number'].unique())
for j in [df01,df15]:
    for i in plots:
        x=j[j['Plot Number']==i]['DGCI_C2'].values.reshape(-1,1)
        y=j[j['Plot Number']==i]['SPAD']
        reg.fit(x,y)
        print(reg.score(x,y))
    print('*'*20)
sns.lmplot(x='DGCI_C2', y='SPAD',hue='Plot Number', data=df15)
plt.show()
# ======================================================================================================
#                               Coefficient of Determination
# =====================================================================================================
#g=sns.barplot(x='models',y='r_square',hue='set',col='input',legend=False,data=df)

g=sns.catplot(x='input',y='r_square',hue='set',kind='bar',col='model',
              legend=False,capsize=0.1,errwidth=1,linewidth=0.5,data=df_nested)
plt.ylim(0.5,0.95)
g.set_axis_labels(x_var="", y_var="Coefficient of determination")
plt.legend(loc='upper right')
plt.tight_layout()
plt.show()
# ======================================================================================================
# SLR and Polynomial model were trained only with DGCI so col='input' would not work here.
my_pal = {"Train":(1,0.75,0.25) , "Test":(0.75,0,0.75)}

#g=sns.catplot(x='model',y='r_square',hue='set',kind='bar',palette=my_pal,
#              legend=False,capsize=0.1,errwidth=1,data=df[df.model=='Polynomial'])
g=sns.catplot(x='input',y='r_square',hue='set',kind='bar', col='model',palette=my_pal,
              legend=False,capsize=0.1,errwidth=1,data=df)
plt.figtext(.5,.95,'', fontsize=10, ha='center')
g.set_axis_labels(x_var="", y_var="Coefficient of determination")  
#g.set_xticklabels(labels=['DGCI'])
plt.legend(loc='upper right')   
plt.ylim(0.5,0.95)
plt.tight_layout()    

plt.show()
'''
#plt.xticks(rotation=70)
#plt.title('Coefficient of determination for DGCI ranges ({} inputs)'.format(var_number))
#figManager = plt.get_current_fig_manager()
#figManager.window.showMaximized()

#g.fig.suptitle('THIS IS A TITLE, YOU BET',y=0.99)
#g.set_titles('Comparison of different models and inputs')
'''
# ======================================================================================================
#                                               Root Mease Square Error
# =====================================================================================================
#RMSE for different models   


# ======================================================================================================
# SLR and Polynomial model were trained only with DGCI so col='input' would not work here.
my_pal = {"DGCI":(1,0.75,0.25) , "Average RGB":(0.75,0,0.75)}
f=sns.catplot(x='input',y='RMSE',kind='bar',col='model',palette=my_pal,
              legend=True,capsize=0.1,errwidth=1,color='#74f0b4',data=df)
f.set_axis_labels(x_var="", y_var="RMSE")   
plt.ylim(2,6.2)
plt.figtext(.5,.98,'input= DGCI', fontsize=10, ha='center')
#plt.legend(loc='upper right')
plt.show()
g.savefig('R2_ML.pdf', format='pdf', dpi=1000)

#===================================================================================================

df_mean=df.groupby(['model','input','set']).mean()
df_mean=df_mean.unstack('set')
df_mean.plot.bar()

df_mean.plot.bar()


plt.close()

sns.barplot(x='model',y='RMSE',data=df)
plt.title('RMSE for DGCI ranges ({} inputs)'.format(var_number))
figManager = plt.get_current_fig_manager()
figManager.window.showMaximized()
plt.show()
plt.savefig('RMSE_{}.png'.format(var_number), format='png', dpi=1000)

#=======================================================================================================
#                       Calculating STD and AVG for models for Rsqr
#=======================================================================================================

df=pd.read_csv(r'./model_scores/AllModels/R_csvFiles/External_cv_all.csv')
aggregated=df.groupby(by=['models','Method','Inputs']).agg(['mean','std'])
aggregated.columns=['Avg','Std']
aggregated.Std=aggregated.Std*2
aggregated.to_csv('aggregated_Rsqr.csv')
#=======================================================================================================
#                       Calculating STD and AVG for models for RMSE
#=======================================================================================================

df=pd.read_csv(r'./model_scores/AllModels/R_csvFiles/External_cv_RMSE.csv')
df.RMSE=np.sqrt(df.RMSE)
aggregated=df.groupby(by=['models','Inputs']).agg(['mean','std'])
aggregated.columns=['Avg','Std']
aggregated.Std=aggregated.Std*2
aggregated.to_csv('aggregated_RMSE.csv')

